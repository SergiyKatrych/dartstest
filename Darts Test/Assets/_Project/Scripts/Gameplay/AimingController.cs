﻿using DartsTest.Helpers.Gameplay;
using DartsTest.Helpers.UI;
using UnityEngine;

namespace DartsTest.Gameplay
{
	public class AimingController : MonoBehaviour
	{
		[SerializeField] private ProgressBar3D progressBar;
		[SerializeField] private float speed = 1;
		private bool _isAiming = false;
		private bool _isIncrementing = true;
		private float progress = 0;

		private DynamicalSettingsHelper.TargetSettingsEntity targetSettings;
		private bool _isInited = false;

		private DynamicalSettingsHelper settings;

		private void OnEnable()
		{
			if (!_isInited)
			{
				Init();
			}
		}

		private void Init()
		{
			_isInited = true;
		}

		public void Show()
		{
			_isIncrementing = true;
			_isAiming = true;
			progress = 0;
			progressBar.CurrentValue = 0;
			progressBar.gameObject.SetActive(true);
		}

		public void Hide()
		{
			progressBar.gameObject.SetActive(false);
			progressBar.CurrentValue = 0;
		}

		public int Shoot()
		{
			_isAiming = false;
			return GetCircle(progress);
		}

		private void Update()
		{
			if (_isAiming)
			{
				float step = speed * Time.deltaTime * (_isIncrementing ? 1 : -1);
				progress += step;
				if (progress > 1)
				{
					_isIncrementing = false;
					progress = 1;
				}
				if (progress < 0)
				{
					_isIncrementing = true;
					progress = 0;
				}

				progressBar.CurrentValue = progress;
			}
		}


		private int GetCircle(float progress)
		{
			int ProgressInPercents = Mathf.RoundToInt(progress * 100);
			int deltaToBullsEye = Mathf.Abs(ProgressInPercents - 50);

			if (deltaToBullsEye < targetSettings.redZone.CalculatedPercentage / 2f)
			{
				return 1;
			}
			else if (deltaToBullsEye < targetSettings.yellowZone.CalculatedPercentage / 2f)
			{
				return 2;
			}
			else if (deltaToBullsEye < targetSettings.greenZone.CalculatedPercentage / 2f)
			{
				return 3;
			}
			else { return 4; }
		}

		public void OnSettingsChanged(DynamicalSettingsHelper settings)
		{
			speed = settings.AimingSpeed;
			targetSettings = settings.TargetSettings;
		}
	}
}