﻿using System;
using UnityEditor;
using UnityEngine;

namespace DartsTest.Helpers.Gameplay
{
	public class DynamicalSettingsHelper : MonoBehaviour
	{
		public float AimingSpeed = 1;
		public TargetSettingsEntity TargetSettings;

		public Action<DynamicalSettingsHelper> OnValueChanged;

		public void Start()
		{
			Recalculate();
		}

		[Serializable]
		public class TargetSettingsEntity
		{
			public TargetData redZone, yellowZone, greenZone, outerSpace;


			public void Recalculate()
			{
				redZone.CalculatedPercentage = redZone.HitPercent;
				yellowZone.CalculatedPercentage = redZone.CalculatedPercentage + yellowZone.HitPercent * 2;
				greenZone.CalculatedPercentage = yellowZone.CalculatedPercentage + greenZone.HitPercent * 2;
				outerSpace.CalculatedPercentage = (100 - greenZone.CalculatedPercentage) / 2;

				int sum = redZone.HitPercent + yellowZone.HitPercent * 2 + greenZone.HitPercent * 2;
				if (sum > 100)
				{
#if UNITY_EDITOR
					EditorUtility.DisplayDialog("Math exception", "Percentage sum should be less 100", "Ok");

#endif
				}
				else
				{
					outerSpace.HitPercent = (100 - sum) / 2;
				}

			}

			[Serializable]
			public struct TargetData
			{
				public int HitPercent;
				public int PointsForHit;
				[HideInInspector]
				public int CalculatedPercentage;
				public Color32 TextColor;
			}
		}


#if UNITY_EDITOR
		private void OnValidate()
		{
			Recalculate();
		}
#endif

		private void Recalculate()
		{
			TargetSettings.Recalculate();
			if (OnValueChanged != null)
			{
				OnValueChanged(this);
			}
		}
	}
}
