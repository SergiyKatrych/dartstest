﻿using DartsTest.Helpers.Gameplay;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshPro))]
public class ScoreController : MonoBehaviour
{
	[SerializeField] private TextMeshPro text;
	float step = 0.1f;
	bool _isFadeOut = false;

	public void Show(DynamicalSettingsHelper.TargetSettingsEntity.TargetData data)
	{
		text.color = data.TextColor;
		var color = text.color;
		color.a = 0;
		text.color = color;

		text.text = data.PointsForHit.ToString();

		gameObject.SetActive(true);
	}

	private void Update()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y + step * Time.deltaTime, transform.position.z);

		Color nextColor = text.color;
		if (_isFadeOut)
		{
			nextColor.a -= step * 0.1f;
		}
		else
		{
			nextColor.a += 2 * step * 0.1f;
		}
		text.color = nextColor;

		if (!_isFadeOut && text.color.a > 1)
		{
			_isFadeOut = true;
		}
		if (_isFadeOut && text.color.a <= 0)
		{
			Destroy(gameObject);
		}
	}
}
