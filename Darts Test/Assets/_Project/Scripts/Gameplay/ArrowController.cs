﻿using System;
using System.Collections;
using DartsTest.Helpers.Gameplay;
using UnityEngine;

namespace DartsTest.Gameplay
{
	public class ArrowController : MonoBehaviour
	{

		[SerializeField] private Vector3 Target;
		[SerializeField] private float speed = 0.01f;
		[SerializeField] private float heightOffset = 0.1f;

		[SerializeField] private ScoreController hitTextPrefab;

		private bool shoudlFallOnGround;
		private DynamicalSettingsHelper.TargetSettingsEntity.TargetData targetData;

		public void Shoot(Vector3 target, bool didHit)
		{
			Target = target;
			shoudlFallOnGround = !didHit;
			StartCoroutine(SimulateParabolicProjectile());
		}

		IEnumerator SimulateLinearProjectile()
		{

			transform.LookAt(Target);

			Vector3 acceleration = (Target - transform.position).normalized * speed;

			bool didReachTarget = false;
			float prevDistance = 100;

			while (!didReachTarget)
			{

				Vector3 nextPos = transform.localPosition + acceleration;
				transform.localPosition = nextPos;

				float newDistance = (Target - transform.position).sqrMagnitude;
				if (newDistance > prevDistance)
				{
					didReachTarget = true;
				}
				prevDistance = newDistance;
				yield return null;
			}
		}


		private Vector3 linearFakePos;
		IEnumerator SimulateParabolicProjectile()
		{
			linearFakePos = transform.position;

			var distance = (Target - transform.position);

			Vector3 step = distance.normalized * speed;

			int stepCounts = Mathf.RoundToInt((Target - transform.position).x / step.x);
			int completedSteps = 0;
			float progress = 0;

			bool didReachTarget = false;
			float prevDistance = 100;

			while (!didReachTarget)
			{

				linearFakePos = linearFakePos + step;
				float cos = Mathf.Sin(Mathf.PI * progress);
				Vector3 verticalOffset = new Vector3(0, cos * heightOffset, 0);

				Vector3 nextPos = linearFakePos + verticalOffset;
				transform.LookAt(nextPos);
				transform.localPosition = nextPos;

				float newDistance = (Target - linearFakePos).sqrMagnitude;


				completedSteps++;
				progress = completedSteps / (float)stepCounts;


				if (progress > 1)
				{
					OnHitTargetPoint();
					didReachTarget = true;
					if (shoudlFallOnGround)
					{
						yield return StartCoroutine(SimulateParabolicProjectileFallToGround());
					}
				}
				prevDistance = newDistance;
				yield return null;
			}
		}

		internal void SetCircleData(DynamicalSettingsHelper.TargetSettingsEntity.TargetData circleData)
		{
			targetData = circleData;
		}

		IEnumerator SimulateParabolicProjectileFallToGround()
		{
			while (transform.position.y > 0)
			{
				transform.Translate(transform.forward * speed);
				yield return null;
			}
		}

		private void OnHitTargetPoint()
		{
			(Instantiate(hitTextPrefab, Target, Quaternion.identity)).Show(targetData);
		}
	}
}