﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace DartsTest.Helpers.UI
{
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(Collider))]

	public class Button3D : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
	{
		[SerializeField] private TextMeshPro buttonText;

		#region Button logic

		public UnityEvent OnClick;

		[SerializeField] private Animator animator;
		private bool _isInteractable = true;

		private bool _isOverObject = false;

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (!_isInteractable) return;
			_isOverObject = true;
			SetState(Constants.UI.ButtonAnimations.Highlighted);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (!_isInteractable) return;
			_isOverObject = false;

			SetState(Constants.UI.ButtonAnimations.Normal);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (!_isInteractable) return;
			SetState(Constants.UI.ButtonAnimations.Pressed);
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if (!_isInteractable) return;
			if (_isOverObject)
			{
				SetState(Constants.UI.ButtonAnimations.Highlighted);
				Click();
			}
		}

		private void SetState(Constants.UI.ButtonAnimations state)
		{
			animator.SetTrigger(state.ToString());

		}

		private void Click()
		{
			if (OnClick != null)
			{
				OnClick.Invoke();
			}
		}

		#endregion

		internal void SetText(string start)
		{
			buttonText.SetText(start);
		}

		internal void SetInteractable(bool isInteractable)
		{
			if (isInteractable)
			{
				SetState(Constants.UI.ButtonAnimations.Normal);
			}
			else
			{
				SetState(Constants.UI.ButtonAnimations.Disabled);
			}

			_isInteractable = isInteractable;
		}
	}
}
