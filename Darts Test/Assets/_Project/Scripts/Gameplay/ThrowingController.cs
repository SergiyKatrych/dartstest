﻿using System;
using DartsTest.Helpers.Gameplay;
using UnityEngine;

namespace DartsTest.Gameplay
{
	public class ThrowingController : MonoBehaviour
	{
		[SerializeField] private TargetController target;

		private Vector3 _targetPos;
		private ArrowController _currentArrow;

		public void Shoot(ArrowController arrow, int circle = 1)
		{
			_currentArrow = arrow;
			_targetPos = target.GetRandomPointAtCircle(circle);
			_currentArrow.Shoot(_targetPos, circle <= 3);
		}
	}
}