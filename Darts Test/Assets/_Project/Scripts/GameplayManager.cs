﻿using DartsTest.Helpers.Gameplay;
using DartsTest.Helpers.UI;
using System.Collections;
using UnityEngine;

namespace DartsTest.Gameplay
{
	public class GameplayManager : MonoBehaviour
	{
		[SerializeField] private Button3D mainButton;
		[SerializeField] private AimingController aimingController;
		[SerializeField] private ThrowingController throwingController;
		[SerializeField] private Transform arrowSpawnObject;
		[SerializeField] private ArrowController arrowPrefab;

		[SerializeField] private DynamicalSettingsHelper settings;

		[SerializeField] private float delaybeforeNextRound = 2f;

		private ArrowController _currentArrow;
		private int _targetCircle = 0;
		#region GameStates implementation
		private enum GameState
		{
			Idle = 0,
			Aming = 1,
			Shooting = 2
		}
		private GameState _currentState;

		public void NextState()
		{
			switch (_currentState)
			{

				case GameState.Idle:
					PrepareAimingState();
					break;
				case GameState.Aming:
					PrepareShootingState();
					break;

				case GameState.Shooting:
				default:
					PrepareIdleState();
					break;
			}
		}
		#endregion

		private void OnEnable()
		{
			settings.OnValueChanged += aimingController.OnSettingsChanged;
		}

		private void OnDisable()
		{
			settings.OnValueChanged -= aimingController.OnSettingsChanged;
		}

		void Start()
		{
			PrepareIdleState();
		}

		#region State Preparation methods
		private void PrepareIdleState()
		{
			aimingController.Hide();

			mainButton.SetInteractable(true);
			mainButton.SetText(Constants.UI.ButtonNames.Start);

			SpawnArrow();
			_currentState = GameState.Idle;
		}

		private void PrepareAimingState()
		{
			mainButton.SetText(Constants.UI.ButtonNames.Shoot);
			aimingController.Show();
			_currentState = GameState.Aming;
		}

		private void PrepareShootingState()
		{
			int circle = aimingController.Shoot();
			_currentArrow.SetCircleData(GetCircleData(circle));
			throwingController.Shoot(_currentArrow, circle);
			_currentState = GameState.Shooting;
			mainButton.SetInteractable(false);
			StartCoroutine(DelayBeforeNewRound());
		}
		#endregion

		public void OnButtonPressed()
		{
			if (_currentState != GameState.Shooting) // we automatically change state after that
			{
				NextState();
			}
		}
	
		private void SpawnArrow()
		{
			_currentArrow = Instantiate(arrowPrefab, arrowSpawnObject.position, arrowSpawnObject.rotation);
		}

		private DynamicalSettingsHelper.TargetSettingsEntity.TargetData GetCircleData(int index)
		{
			switch (index)
			{
				case 1: return settings.TargetSettings.redZone;
				case 2: return settings.TargetSettings.yellowZone;
				case 3: return settings.TargetSettings.greenZone;
				default: return settings.TargetSettings.outerSpace;
			}
		}

		private IEnumerator DelayBeforeNewRound()
		{
			yield return new WaitForSeconds(delaybeforeNextRound);
			NextState();
		}
	}
}