﻿using UnityEditor;
using UnityEngine;

namespace DartsTest.Gameplay
{
	public class TargetController : MonoBehaviour
	{
		[SerializeField] private Transform targetCenterObject;
		[SerializeField] private float circleRadius = 2.2f;
		[SerializeField] private GameObject tempSpawnPrefab;

		[SerializeField] private ScoreController hitTextPrefab;

		public int circle = 1;

		public void SpawnAtCurrentCircle()
		{
			GetRandomPointAtCircle(circle);
		}

		public Vector3 GetRandomPointAtCircle(int circle = 0)
		{
			float angle = Random.Range(0f, Mathf.PI * 2f);
			Vector3 point = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0);

			point = point * Random.Range(circle - 1f, circle) * circleRadius;
			point.Scale(targetCenterObject.lossyScale);

			point = RotatePointAroundPivot(targetCenterObject.position + point, targetCenterObject.position, targetCenterObject.rotation.eulerAngles);

			SpawnSphere(point);
			return point;
		}

		private void SpawnSphere(Vector3 position)
		{

			Instantiate(tempSpawnPrefab, position, Quaternion.identity);
		}

		private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
		{
			Vector3 dir = point - pivot; // get point direction relative to pivot
			dir = Quaternion.Euler(angles) * dir; // rotate it
			point = dir + pivot; // calculate rotated point
			return point; // return it
		}


#if UNITY_EDITOR
		void OnDrawGizmosSelected()
		{
			Handles.color = Color.cyan;
			for (int i = 0; i < 3; i++)
			{
				Handles.DrawWireDisc(targetCenterObject.position, targetCenterObject.forward, circleRadius * transform.lossyScale.x * i);
			}
		}

#endif
	}
}