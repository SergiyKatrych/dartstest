﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
	public struct UI
	{
		public enum ButtonAnimations
		{
			Normal = 0,
			Highlighted = 1,
			Pressed = 2,
			Selected = 3,
			Disabled = 4
		}

		public struct ButtonNames
		{
			public const string Start = "START";
			public const string Shoot = "SHOOT";
		}
	}
}
