﻿using UnityEditor;
using UnityEngine;

namespace DartsTest.Helpers.UI
{
	[RequireComponent(typeof(MeshRenderer))]
	public class ProgressBar3D : MonoBehaviour
	{
		[SerializeField] private float elementWidth = 1;
		[SerializeField] private Transform pointer;

		[Range(0, 1)]
		public float CurrentValue = 0;//{ get; set; }

		private Vector3 minPosition;
		private float realWidth;
		private bool _isInited = false;

		void OnEnable()
		{
			if (!_isInited)
			{
				Init();
			}
		}

		private void Init()
		{
			_isInited = true;
			realWidth = elementWidth / 2f;

			minPosition = pointer.localPosition;
			minPosition = new Vector3(minPosition.x - realWidth / 2f, minPosition.y, minPosition.z);
		}

		void Update()
		{
			UpdatePointerPosition();
		}

		private void UpdatePointerPosition()
		{
			pointer.localPosition = new Vector3(minPosition.x + realWidth * CurrentValue, minPosition.y, minPosition.z);
		}

#if UNITY_EDITOR
		void OnDrawGizmosSelected()
		{
			Handles.color = Color.cyan;
			Gizmos.DrawWireCube(transform.position, new Vector3(elementWidth, 0.03f, 0.01f));
		}
#endif
	}
}